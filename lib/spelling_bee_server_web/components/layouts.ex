defmodule SpellingBeeServerWeb.Layouts do
  use SpellingBeeServerWeb, :html

  embed_templates "layouts/*"
end
