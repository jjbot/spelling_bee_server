defmodule SpellingBeeServerWeb.GameJSON do
  def index(%{game: %{letters: letters, focus_letter: fl}}) do
    %{game: %{"focus_letter" => fl, "letters" => letters}}
  end

  def check(%{response: reply}) do
    reply
  end
end
