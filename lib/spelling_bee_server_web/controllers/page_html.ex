defmodule SpellingBeeServerWeb.PageHTML do
  use SpellingBeeServerWeb, :html

  embed_templates "page_html/*"
end
