defmodule SpellingBeeServerWeb.GameController do
  use SpellingBeeServerWeb, :controller

  def index(conn, _params) do
    render(conn, :index, game: Bee.get_game())
  end

  def check(conn, %{"answer" => answer} = params) do
    IO.inspect(params)
    valid_score = Bee.check_answer(answer)
    render(conn, :check, response: valid_score)
  end
end
