defmodule Bee do
  @moduledoc """
  """

  # focus letter
  # letters
  # pangrams
  # focus word (which is a pangram)
  # solutions
  #
  # scores:
  # 4 letters: 1
  # 5 and above, length of the word
  # pangram: length of the word + 7

  use GenServer

  @enforce_keys [:focus_letter, :letters, :word_list]
  defstruct [:focus_letter, :letters, :word_list]

  @type t :: %__MODULE__{
    focus_letter: String.t(),
    letters: [String.t()],
    word_list: [String.t()]
  }

  ###############################################################################
  # Public API
  ###############################################################################

  def start_link(_) do
    GenServer.start_link(Bee, nil, name: Bee)
  end

  @doc """
  Start a new game.

  This reloads the full word list, selects a new pangram to act as the focus_word
  and sets the letters accordingly.
  """
  def reset_game() do
    GenServer.call(Bee, :reset_game)
  end

  @doc """
  Get all info related to the game that is relevant for the FE.
  """
  def get_game() do
    GenServer.call(Bee, :get_game)
  end

  @doc """
  Get all the letters of the current game.
  """
  def get_letters() do
    GenServer.call(Bee, :get_letters)
  end

  @doc """
  Get the current list of valid words.
  """
  def get_words() do
    GenServer.call(Bee, :get_words)
  end

  @doc """
  Check if the provided `word` is a valid answer.
  """
  def check_answer(word) do
    GenServer.call(Bee, {:check_answer, word})
  end

  ###############################################################################
  # GenServer callbacks
  ###############################################################################

  @impl GenServer
  def init(_) do
    {:ok, initialise_game()}
  end

  @impl GenServer
  def handle_call(:reset_game, _, _) do
    {:reply, :ok, initialise_game()}
  end

  def handle_call(:get_game, _, %Bee{focus_letter: fl, letters: letters} = state) do
    {:reply, %{focus_letter: fl, letters: letters}, state}
  end

  def handle_call(:get_letters, _, %Bee{letters: letters} = state) do
    {:reply, letters, state}
  end

  def handle_call(:get_words, _, %Bee{word_list: words} = state) do
    {:reply, words, state}
  end

  def handle_call({:check_answer, answer}, _, %Bee{focus_letter: fl, letters: letters, word_list: words} = state) do
    correct = contains_focus_letter?(fl, answer) and only_pangram_letters?(letters, answer) and valid_word?(words, answer)

    reply =
      if correct do
        s = MapSet.new(String.split(answer, "", trim: true))
        score =
          case {String.length(answer), MapSet.size(s)} do
            {4, _} -> 4
            {x, 7} when x > 4 -> x + 7
            {x, _} -> x
          end
        %{"valid" => true, "score" => score}
      else
        %{"valid" => false, "score" => 0}
      end

    {:reply, reply, state}
  end

  ###############################################################################
  # Private helper functions
  ###############################################################################

  # Checks if the supplied answer contains the focus letter
  defp contains_focus_letter?(fl, answer) do
    String.contains?(answer, fl)
    |> IO.inspect(label: "Contains focus letter")
  end

  # Checks if the word is made up only of letters present in the pangram
  defp only_pangram_letters?(letters, answer) do
    answer
    |> String.split("", trim: true)
    |> Enum.all?(fn c -> c in letters end)
    |> IO.inspect(label: "All letters in list")
  end

  # Check if the word is present in the list of permitted words.
  defp valid_word?(words, answer) do
    word_known = answer in words
    IO.inspect(word_known, label: "Word in list")
    word_known
  end

  defp initialise_game() do
    words = get_words_from_file()
    focus_word = get_focus_word(words)
    filtered_words = filter_word_list(words, focus_word)
    [focus_letter | _] = letters =
      focus_word
      |> String.split("", trim: true)
      |> Enum.shuffle()
    %Bee{focus_letter: focus_letter, letters: letters, word_list: filtered_words}
  end

  # Read words for a text file with one word per line.
  # FIXME: should take an argument to indicate which file to read.
  defp get_words_from_file() do
    File.read!("priv/static/words_alpha.txt")
    |> String.split("\n", trim: true)
    |> Enum.map(&String.trim/1)
    |> Enum.filter(fn s -> String.length(s) >= 4 end)
  end

  # Limits the word list to words that only contain letters present in the pangram.
  defp filter_word_list(words, focus_word) do
    check_set = MapSet.new(String.split(focus_word, "", trim: true))

    Enum.filter(
      words,
      fn word ->
        MapSet.new(String.split(word, "", trim: true))
        |> MapSet.subset?(check_set)
      end
    )
  end

  # Randomly select a pangram to serve as the focus word from the list of words.
  defp get_focus_word(words) do
    words
    |> Enum.filter(fn w -> String.length(w) == 7 end)
    |> Enum.map(fn w -> {w, MapSet.new(String.split(w, "", trim: true))} end)
    |> Enum.filter(fn {_w, s} -> MapSet.size(s) == 7 end)
    |> Enum.map(fn {w, _s} -> w end)
    |> Enum.random()
  end
end
